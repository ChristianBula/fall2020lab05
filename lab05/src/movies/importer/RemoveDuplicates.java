// Oleg Christian Bula 1935095

package movies.importer;
import java.util.ArrayList;

public class RemoveDuplicates extends Processor {
		public RemoveDuplicates(String srcDir,String outDir) {
			super(srcDir,outDir,false);
		}
		public ArrayList<String> process(ArrayList<String> array){
			ArrayList<String> noDuplicates = new ArrayList<String>();
			for(int i = 0;i < array.size();i++) {
				if(!noDuplicates.contains(array.get(i))) {
					noDuplicates.add(array.get(i));
				}
			}
			return noDuplicates;
		}
}
