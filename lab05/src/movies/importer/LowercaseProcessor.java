// Oleg Christian Bula 1935095

package movies.importer;
import java.util.ArrayList;

public class LowercaseProcessor extends Processor{
	public LowercaseProcessor(String srcDir,String outDir) {
		super(srcDir,outDir,true);
	}
	public ArrayList<String> process(ArrayList<String> array) {
		ArrayList<String> asLower = new ArrayList<String>();
		for(String elem : array) {
		    asLower.add(elem.toLowerCase()); 
		}
		return asLower;
	}

}
